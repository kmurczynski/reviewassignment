using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace kmurczynski.MapGenerator
{
    // TODO: Make editor window out of this. Left MonoBehaviour for sake of time saving.
    public class Generator : MonoBehaviour
    {
        // TODO: Implement attribute to displaying all avalaible settings in dropdown list.
        [SerializeField]
        private BaseMapSettingsSO mapSettings;
        // TODO: If user will have access to seed, rewrite this to any type of input.
        [SerializeField]
        private int seed;

        private bool isLocked = false;
        private CancellationTokenSource cts;
        // TODO: Delate it after testing
        private IMapData lastMapData;

        public async void GenerateMapAsync()
        {
            // TODO: Test generating multipla maps in the same time.
            if(isLocked)
            {
                Debug.LogWarning("One geretation operation already running. Exiting command");
                return;
            }

            cts = new CancellationTokenSource();
            CancellationToken token = cts.Token;
            try
            {
                // TODO: Allow selecting of engine to designer/player/modder
                BaseEngine engine = new BaseEngine();
                IMapData mapData = Task.Run(() => engine.GenerateMap(mapSettings, seed)).Result;
                lastMapData = mapData;
            }
            catch (OperationCanceledException exc)
            {
                if (gameObject != null)
                {
                    Debug.LogError("Map generating canceled", gameObject);
                }
                else
                {
                    Debug.LogError("Map generating canceled");
                }
            }
            finally
            {
                cts.Dispose();
            }

            cts = null;
        }

        public void CancelMapGeneration()
        {
            cts?.Cancel();
        }

        public void GenerateRandomSeed()
        {
            seed = Math.Abs(UnityEngine.Random.Range(int.MinValue, int.MaxValue));
        }

        private void TestPrintGeneratedData()
        {

        }

        private void OnDestroy()
        {
            CancelMapGeneration();
        }


    }
}
