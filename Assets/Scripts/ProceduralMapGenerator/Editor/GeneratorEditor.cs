using UnityEngine;
using UnityEditor;

namespace kmurczynski.MapGenerator.Editor
{
    [CustomEditor(typeof(Generator))]
    public class GeneratorEditor : UnityEditor.Editor
    {
        private readonly float MediumButtonHeight = 30f;


        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            Generator generator = (Generator)target;

            EditorGUILayout.Space();

            if (GUILayout.Button("Generate random seed", GUILayout.Height(MediumButtonHeight)))
            {
                Debug.LogWarning("Generatin random seed not implemented at this moment");
                generator.GenerateRandomSeed();
            }

            EditorGUILayout.Space();

            if (GUILayout.Button("Generate map", GUILayout.Height(MediumButtonHeight)))
            {
                generator.GenerateMapAsync();
            }
        }
    }
}
