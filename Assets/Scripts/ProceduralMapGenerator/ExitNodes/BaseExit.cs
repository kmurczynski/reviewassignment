using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace kmurczynski.MapGenerator
{
    public class BaseExit : MonoBehaviour, IExit
    {
        public Vector3 SpawnPosition { get => spawnPosition; set => spawnPosition = value; }
        public float ElementSize => elementSize;

        [SerializeField]
        private Vector3 spawnPosition;
        // TODO: This variable should could be calculated automatically.
        [SerializeField]
        private float elementSize;
    }
}
