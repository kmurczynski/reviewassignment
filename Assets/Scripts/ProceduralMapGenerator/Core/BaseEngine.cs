using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace kmurczynski.MapGenerator
{
    public class BaseEngine
    {
        public IMapData GenerateMap(IMapSettings mapSettings, int seed)
        {
            List<IElement> allElements = new List<IElement>();
            IDoor doors = mapSettings.GetRandomDoor(seed);
            allElements.Add(doors);
            List<IRoom> rooms = mapSettings.GetRandomRooms(seed);
            allElements.AddRange(rooms);
            List<IExit> exits = mapSettings.GetRandomExits(seed);
            allElements.AddRange(exits);

            mapSettings.MapShape.FindAndFillElementsPositions(rooms, exits, doors, seed);

            // TODO: This line should be rewritten to allow user using different IMapData's
            IMapData mapData = new MapData(allElements.ToArray());

            return mapData;
        }
    }
}
