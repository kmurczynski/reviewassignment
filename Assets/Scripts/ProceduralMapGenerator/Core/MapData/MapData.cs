using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace kmurczynski.MapGenerator
{
    public class MapData : IMapData
    {
        public IElement[] MapElements { get; set; }

        public MapData()
        {
        }
        public MapData(IElement[] mapElements)
        {
            MapElements = mapElements ?? throw new ArgumentNullException(nameof(mapElements));
        }
    }
}
