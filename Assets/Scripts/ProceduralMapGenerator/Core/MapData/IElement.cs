using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace kmurczynski.MapGenerator
{
    public interface IElement 
    {
        Vector3 SpawnPosition { get; set; }
        // TODO: Rewrite this to use bounds instead of float.
        float ElementSize { get; }

    }
}
