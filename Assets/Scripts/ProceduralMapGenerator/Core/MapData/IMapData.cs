using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace kmurczynski.MapGenerator
{
    public interface IMapData
    {
        IElement[] MapElements { get; set; }
    }
}
