using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace kmurczynski.MapGenerator
{
    public abstract class BaseShape : IShape
    {
        public abstract int MinimalAmountOfElements { get; }

        public abstract void FindAndFillElementsPositions(IEnumerable<IRoom> rooms, IEnumerable<IExit> exits, IDoor doors, int seed);
    }
}
