using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace kmurczynski.MapGenerator
{
    public interface IShape
    {
        int MinimalAmountOfElements { get; }
        void FindAndFillElementsPositions(IEnumerable<IRoom> rooms, IEnumerable<IExit> exits, IDoor doors, int seed);
    }
}
