using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace kmurczynski.MapGenerator
{
    public class TShape : BaseShape
    {
        private readonly float HorizontalTSectorRatio = 1;
        private readonly float VetricalTSectorRatio = 1;

        public override int MinimalAmountOfElements => 6;

        // TODO: Add offset to spawn position
        public override void FindAndFillElementsPositions(IEnumerable<IRoom> rooms, IEnumerable<IExit> exits, IDoor doors, int seed)
        {
            int roomsCount = rooms.Count();

            if(roomsCount < MinimalAmountOfElements)
            {
                Debug.LogError("Not enough elements to generate map, abadoning");
                return;
            }

            int segmentMultiplier = Mathf.FloorToInt(roomsCount / (HorizontalTSectorRatio + VetricalTSectorRatio));
            int horizontalElementsCount = (int)(HorizontalTSectorRatio * segmentMultiplier);
            horizontalElementsCount = horizontalElementsCount % 2 == 0 ? horizontalElementsCount : horizontalElementsCount + 1;
            int verticalElementsCount = roomsCount - horizontalElementsCount;

            int verticalRowStartIndex = Mathf.CeilToInt(horizontalElementsCount / 2);
            Vector3 verticalStartEdgePosition = Vector3.zero;
            Vector3 spawnEdgePosition = Vector3.zero;

            // TODO: Rewrite this algorithm.
            for (int i = 0; i < horizontalElementsCount; i++)
            {
                //spawnEdgePosition = GetNewEdgePosition(spawnEdgePosition, )

                if (i == verticalRowStartIndex)
                {
                    
                }
            }
        }

        // TODO: Maybe give user ability to change vector of drawing?
        private Vector3 GetNewEdgePosition(Vector3 position, float segmentSize)
        {
            return new Vector3(position.x + segmentSize, position.y, position.z + segmentSize);
        }
    }
}
