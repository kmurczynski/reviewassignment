using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace kmurczynski.MapGenerator
{
    [CreateAssetMenu(fileName = "MapSettingsSO", menuName = "ScriptableObject/Create Map settings scriptable object", order = 1)]
    public class BaseMapSettingsSO : ScriptableObject, IMapSettings
    {
        public int RoomsQuantity => roomsQuantity;
        public int ExitsQuantity => exitsQuantity;
        public IShape MapShape => MapShape;

        public IRoom[] RoomElementsPool => roomElementsPool;
        public IExit[] ExitElementsPool => exitElementsPool;
        public IDoor[] DoorElementsPool => doorElementsPool;

        [SerializeField]
        private int roomsQuantity;
        [SerializeField]
        private int exitsQuantity;
        [SerializeField]
        private BaseShape mapShape;

        [SerializeField]
        private BaseRoom[] roomElementsPool;
        [SerializeField]
        private BaseExit[] exitElementsPool;
        [SerializeField]
        private BaseDoor[] doorElementsPool;



        public List<IRoom> GetRandomRooms(int seed)
        {
            // TODO: Generate exit using seed for random

            List<IRoom> exits = new List<IRoom>();

            // TODO: In the future, when there will be larger pool of objects
            // remove already used elements from random 
            for (int i = 0; i < exitsQuantity; i++)
            {
                int choosenRoomId = Random.Range(0, RoomElementsPool.Length - 1);
                exits.Add(RoomElementsPool[choosenRoomId]);
            }

            return exits;
        }
        public IDoor GetRandomDoor(int seed)
        {
            // TODO: Generate exit using seed for random

            int choosenDoorId = Random.Range(0, DoorElementsPool.Length);
            return DoorElementsPool[choosenDoorId];
        }
        public List<IExit> GetRandomExits(int seed)
        {
            // TODO: Generate exit using seed for random

            List<IExit> exits = new List<IExit>();

            // TODO: In the future, when there will be larger pool of objects
            // remove already used elements from random 
            for (int i = 0; i < exitsQuantity; i++)
            {
                int choosenExitId = Random.Range(0, ExitElementsPool.Length);
                exits.Add(ExitElementsPool[choosenExitId]);
            }

            return exits;
        }
    }
}
