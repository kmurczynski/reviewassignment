using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace kmurczynski.MapGenerator
{
    public interface IMapSettings
    {
        int RoomsQuantity { get; }
        int ExitsQuantity { get; }
        IShape MapShape { get; }

        IRoom[] RoomElementsPool { get; }
        IExit[] ExitElementsPool { get; }
        IDoor[] DoorElementsPool { get; }

        List<IRoom> GetRandomRooms(int seed);
        IDoor GetRandomDoor(int seed);
        List<IExit> GetRandomExits(int seed);
    }
}
